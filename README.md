# Gobytes

Golang helpers for data sizes

Originally forked from [c2h5oh/datasize](https://github.com/c2h5oh/datasize) and added some function docs and more test coverage.

## Install

```shell
$ dep ensure -add gitlab.com/haath/gobytes
```

## Usage

### Constants

Just like `time` package provides `time.Second`, `time.Day` constants `gobytes` provides:

* `gobytes.B` 1 byte
* `gobytes.KB` 1 kilobyte
* `gobytes.MB` 1 megabyte
* `gobytes.GB` 1 gigabyte
* `gobytes.TB` 1 terabyte
* `gobytes.PB` 1 petabyte
* `gobytes.EB` 1 exabyte

And also the power-2 units

* `gobytes.KiB`
* `gobytes.MiB`
* `gobytes.GiB`
* `gobytes.TiB`
* `gobytes.PiB`
* `gobytes.EiB`

### Helpers

Just like `time` package provides `duration.Nanoseconds() uint64 `, `duration.Hours() float64` helpers `gobytes` has

* `ByteSize.Bytes() uint64`
* `ByteSize.KBytes() float4`
* `ByteSize.MBytes() float64`
* `ByteSize.GBytes() float64`
* `ByteSize.TBytes() float64`
* `ByteSize.PBytes() float64`
* `ByteSize.EBytes() float64`

Warning: see limitations at the end of this document about a possible precission loss

### Parsing strings

`gobytes.ByteSize` implements `TextUnmarshaler` interface and will automatically parse human readable strings into correct values where it is used:

* `"10 MB"` -> `10* gobytes.MB`
* `"10240 g"` -> `10 * gobytes.TB`
* `"2000"` -> `2000 * gobytes.B`
* `"1tB"` -> `gobytes.TB`
* `"5 peta"` -> `5 * gobytes.PB`
* `"28 kilobytes"` -> `28 * gobytes.KB`
* `"1 gigabyte"` -> `1 * gobytes.GB`
* `"3 MiB"` -> `3 * gobytes.MiB`

You can also do it manually:

```go
var v gobytes.ByteSize
err := v.UnmarshalText([]byte("100 mb"))
```

### Printing

`Bytesize.String()` uses largest unit allowing an integer value:
    * `(102400 * gobytes.MB).String()` -> `"100GB"`
    * `(gobytes.MB + gobytes.KB).String()` -> `"1025KB"`

Use `%d` format string to get value in bytes without a unit

### JSON and other encoding

Both `TextMarshaler` and `TextUnmarshaler` interfaces are implemented - JSON will just work. Other encoders will work provided they use those interfaces.

### Human readable

`ByteSize.HumanReadable()` or `ByteSize.HR()` returns a string with 1-3 digits, followed by 1 decimal place, a space and unit big enough to get 1-3 digits

    * `(102400 * gobytes.MB).String()` -> `"100.0 GB"`
    * `(gobytes.MB + 512 * gobytes.KB).String()` -> `"1.5 MB"`

### Limitations

* The underlying data type for `data.ByteSize` is `uint64`, so values outside of 0 to 2^64-1 range will overflow
* size helper functions (like `ByteSize.Kilobytes()`) return `float64`, which can't represent all possible values of `uint64` accurately:
  * if the returned value is supposed to have no fraction (ie `(10 * gobytes.MB).Kilobytes()`) accuracy loss happens when value is more than 2^53 larger than unit: `.Kilobytes()` over 8 petabytes, `.Megabytes()` over 8 exabytes
  * if the returned value is supposed to have a fraction (ie `(gobytes.PB + gobytes.B).Megabytes()`) in addition to the above note accuracy loss may occur in fractional part too - larger integer part leaves fewer bytes to store fractional part, the smaller the remainder vs unit the move bytes are required to store the fractional part
* Parsing a string with `Mb`, `Tb`, etc units will return a syntax error, because capital followed by lower case is commonly used for bits, not bytes
* Parsing a string with value exceeding 2^64-1 bytes will return 2^64-1 and an out of range error
