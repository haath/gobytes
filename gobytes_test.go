package gobytes

import (
	"testing"
)

func TestMarshalText(t *testing.T) {
	table := []struct {
		in  ByteSize
		out string
	}{
		{0, "0B"},
		{B, "1B"},
		{KiB, "1KB"},
		{MiB, "1MB"},
		{GiB, "1GB"},
		{TiB, "1TB"},
		{PiB, "1PB"},
		{EiB, "1EB"},
		{400 * TiB, "400TB"},
		{2048 * MiB, "2GB"},
		{B + KiB, "1025B"},
		{MiB + 20*KiB, "1044KB"},
		{100*MiB + KiB, "102401KB"},
	}

	for _, tt := range table {
		b, _ := tt.in.MarshalText()
		s := string(b)

		if s != tt.out {
			t.Errorf("MarshalText(%d) => %s, want %s", tt.in, s, tt.out)
		}
	}
}

func TestUnmarshalText(t *testing.T) {
	table := []struct {
		in  string
		err bool
		out ByteSize
	}{
		{"0", false, ByteSize(0)},
		{"0B", false, ByteSize(0)},
		{"0 KB", false, ByteSize(0)},
		{"1", false, B},
		{"1KiB", false, KiB},
		{"1 KB", false, 1000},
		{"2MiB", false, 2 * MiB},
		{"2MB", false, 2 * 1000000},
		{"5 GiB", false, 5 * GiB},
		{"20480 GiB", false, 20 * TiB},
		{"50 eB", true, ByteSize((1 << 64) - 1)},
		{"200000 pb", true, ByteSize((1 << 64) - 1)},
		{"10 Mb", true, ByteSize(0)},
		{"g", true, ByteSize(0)},
		{"10 kiB ", false, 10 * KiB},
		{"10 kBs ", true, ByteSize(0)},
	}

	for _, tt := range table {
		var s ByteSize
		err := s.UnmarshalText([]byte(tt.in))

		if (err != nil) != tt.err {
			t.Errorf("UnmarshalText(%s) => %v, want no error", tt.in, err)
		}

		if s != tt.out {
			t.Errorf("UnmarshalText(%s) => %d bytes, want %d bytes", tt.in, s, tt.out)
		}
	}
}

func TestBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out uint64
	}{
		{10, 10},
	}

	for _, tt := range table {

		s := tt.in.Bytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestKBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000, 9.765625},
	}

	for _, tt := range table {

		s := tt.in.KbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestMBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000000, 9.5367431640625},
	}

	for _, tt := range table {

		s := tt.in.MbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestGBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000000000, 9.313225746154785},
	}

	for _, tt := range table {

		s := tt.in.GbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestTBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000000000000, 9.094947017729282},
	}

	for _, tt := range table {

		s := tt.in.TbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestPBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000000000000000, 8.881784197001252},
	}

	for _, tt := range table {

		s := tt.in.PbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestEBytes(t *testing.T) {
	table := []struct {
		in  ByteSize
		out float64
	}{
		{10000000000000000000, 8.673617379884035},
	}

	for _, tt := range table {

		s := tt.in.EbBytes()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}
}

func TestHumanReadable(t *testing.T) {
	table := []struct {
		in  ByteSize
		out string
	}{
		{3, "3 B"},
		{5000, "4.9 KB"},
		{5200, "5.1 KB"},
		{5220, "5.1 KB"},
		{5220000, "5.0 MB"},
		{5220000000, "4.9 GB"},
		{5220000000000, "4.7 TB"},
		{5220000000000000, "4.6 PB"},
		{5220000000000000000, "4.5 EB"},
	}

	for _, tt := range table {

		s := tt.in.HumanReadable()

		if s != tt.out {
			t.Errorf("got %v want %v", s, tt.out)
		}
	}

}
