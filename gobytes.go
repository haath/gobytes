package gobytes

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// ByteSize is a number, represting a size in bytes.
type ByteSize uint64

const (
	// B equals byte
	B ByteSize = 1
	// KiB equals one kilobibyte
	KiB = B << 10
	// KB equals one kilobyte
	KB = B * 1000
	// MiB equals one mebibyte
	MiB = KiB << 10
	// MB equals one megabyte
	MB = KB * 1000
	// GiB equals one gibibyte
	GiB = MiB << 10
	// GB equals one gigabyte
	GB = MB * 1000
	// TiB equals one tebibyte
	TiB = GiB << 10
	// TB equals one terrabyte
	TB = GB * 1000
	// PiB equals one repibyte
	PiB = TiB << 10
	// PB equals one petabyte
	PB = TB * 1000
	// EiB equals one exbibyte
	EiB = PiB << 10
	// EB equals one exabyte
	EB = PB * 1000

	fnUnmarshalText string = "UnmarshalText"
	maxUint64       uint64 = (1 << 64) - 1
	cutoff          uint64 = maxUint64 / 10
)

var errBits = errors.New("unit with capital unit prefix and lower case unit (b) - bits, not bytes ")

// Bytes returns the size in bytes.
func (b ByteSize) Bytes() uint64 {
	return uint64(b)
}

// KbBytes returns the size in kibibytes.
func (b ByteSize) KbBytes() float64 {
	v := b / KiB
	r := b % KiB
	return float64(v) + float64(r)/float64(KiB)
}

// KBytes returns the size in kilobytes.
func (b ByteSize) KBytes() float64 {
	v := b / KB
	r := b % KB
	return float64(v) + float64(r)/float64(KB)
}

// MbBytes returns the size in mebibytes.
func (b ByteSize) MbBytes() float64 {
	v := b / MiB
	r := b % MiB
	return float64(v) + float64(r)/float64(MiB)
}

// MBytes returns the size in megabytes.
func (b ByteSize) MBytes() float64 {
	v := b / MB
	r := b % MB
	return float64(v) + float64(r)/float64(MB)
}

// GbBytes returns the size in gibibytes.
func (b ByteSize) GbBytes() float64 {
	v := b / GiB
	r := b % GiB
	return float64(v) + float64(r)/float64(GiB)
}

// GBytes returns the size in gigabytes.
func (b ByteSize) GBytes() float64 {
	v := b / GB
	r := b % GB
	return float64(v) + float64(r)/float64(GB)
}

// TbBytes returns the size in tebibytes.
func (b ByteSize) TbBytes() float64 {
	v := b / TiB
	r := b % TiB
	return float64(v) + float64(r)/float64(TiB)
}

// TBytes returns the size in terabytes.
func (b ByteSize) TBytes() float64 {
	v := b / TB
	r := b % TB
	return float64(v) + float64(r)/float64(TB)
}

// PbBytes returns the size in pebibytes.
func (b ByteSize) PbBytes() float64 {
	v := b / PiB
	r := b % PiB
	return float64(v) + float64(r)/float64(PiB)
}

// PBytes returns the size in petabytes.
func (b ByteSize) PBytes() float64 {
	v := b / PB
	r := b % PB
	return float64(v) + float64(r)/float64(PB)
}

// EbBytes returns the size in exbibytes.
func (b ByteSize) EbBytes() float64 {
	v := b / EiB
	r := b % EiB
	return float64(v) + float64(r)/float64(EiB)
}

// EBytes returns the size in exabytes.
func (b ByteSize) EBytes() float64 {
	v := b / EB
	r := b % EB
	return float64(v) + float64(r)/float64(EB)
}

func (b ByteSize) String() string {
	switch {
	case b == 0:
		return fmt.Sprint("0B")
	case b%EiB == 0:
		return fmt.Sprintf("%dEB", b/EiB)
	case b%PiB == 0:
		return fmt.Sprintf("%dPB", b/PiB)
	case b%TiB == 0:
		return fmt.Sprintf("%dTB", b/TiB)
	case b%GiB == 0:
		return fmt.Sprintf("%dGB", b/GiB)
	case b%MiB == 0:
		return fmt.Sprintf("%dMB", b/MiB)
	case b%KiB == 0:
		return fmt.Sprintf("%dKB", b/KiB)
	default:
		return fmt.Sprintf("%dB", b)
	}
}

// HumanReadable returns the size in a human-readable string.
func (b ByteSize) HumanReadable() string {
	switch {
	case b > EB:
		return fmt.Sprintf("%.1f EB", b.EbBytes())
	case b > PB:
		return fmt.Sprintf("%.1f PB", b.PbBytes())
	case b > TB:
		return fmt.Sprintf("%.1f TB", b.TbBytes())
	case b > GB:
		return fmt.Sprintf("%.1f GB", b.GbBytes())
	case b > MB:
		return fmt.Sprintf("%.1f MB", b.MbBytes())
	case b > KB:
		return fmt.Sprintf("%.1f KB", b.KbBytes())
	default:
		return fmt.Sprintf("%d B", b)
	}
}

// MarshalText converts the size to bytes.
func (b ByteSize) MarshalText() ([]byte, error) {
	return []byte(b.String()), nil
}

// UnmarshalText parses marshaled bytes into this datasize.
func (b *ByteSize) UnmarshalText(t []byte) error {
	var val uint64
	var unit string

	// copy for error message
	t0 := t

	var c byte
	var i int

ParseLoop:
	for i < len(t) {
		c = t[i]
		switch {
		case '0' <= c && c <= '9':
			if val > cutoff {
				goto Overflow
			}

			c = c - '0'
			val *= 10

			if val > val+uint64(c) {
				// val+v overflows
				goto Overflow
			}
			val += uint64(c)
			i++

		default:
			if i == 0 {
				goto SyntaxError
			}
			break ParseLoop
		}
	}

	unit = strings.TrimSpace(string(t[i:]))
	switch unit {
	case "Kb", "Mb", "Gb", "Tb", "Pb", "Eb":
		goto BitsError
	}
	unit = strings.ToLower(unit)
	switch unit {
	case "", "b", "byte":
		// do nothing - already in bytes

	case "k", "kb", "kilo", "kilobyte", "kilobytes":
		if val > maxUint64/uint64(KB) {
			goto Overflow
		}
		val *= uint64(KB)

	case "kib":
		if val > maxUint64/uint64(KiB) {
			goto Overflow
		}
		val *= uint64(KiB)

	case "m", "mb", "mega", "megabyte", "megabytes":
		if val > maxUint64/uint64(MB) {
			goto Overflow
		}
		val *= uint64(MB)

	case "mib":
		if val > maxUint64/uint64(MiB) {
			goto Overflow
		}
		val *= uint64(MiB)

	case "g", "gb", "giga", "gigabyte", "gigabytes":
		if val > maxUint64/uint64(GB) {
			goto Overflow
		}
		val *= uint64(GB)

	case "gib":
		if val > maxUint64/uint64(GiB) {
			goto Overflow
		}
		val *= uint64(GiB)

	case "t", "tb", "tera", "terabyte", "terabytes":
		if val > maxUint64/uint64(TB) {
			goto Overflow
		}
		val *= uint64(TB)

	case "tib":
		if val > maxUint64/uint64(TiB) {
			goto Overflow
		}
		val *= uint64(TiB)

	case "p", "pb", "peta", "petabyte", "petabytes":
		if val > maxUint64/uint64(PB) {
			goto Overflow
		}
		val *= uint64(PB)

	case "pib":
		if val > maxUint64/uint64(PiB) {
			goto Overflow
		}
		val *= uint64(PiB)

	case "e", "eb", "exa", "exabyte", "exabytes":
		if val > maxUint64/uint64(EB) {
			goto Overflow
		}
		val *= 1000000000000000000

	case "eib":
		if val > maxUint64/uint64(EiB) {
			goto Overflow
		}
		val *= uint64(EiB)

	default:
		goto SyntaxError
	}

	*b = ByteSize(val)
	return nil

Overflow:
	*b = ByteSize(maxUint64)
	return &strconv.NumError{Func: fnUnmarshalText, Num: string(t0), Err: strconv.ErrRange}

SyntaxError:
	*b = 0
	return &strconv.NumError{Func: fnUnmarshalText, Num: string(t0), Err: strconv.ErrSyntax}

BitsError:
	*b = 0
	return &strconv.NumError{Func: fnUnmarshalText, Num: string(t0), Err: errBits}
}
